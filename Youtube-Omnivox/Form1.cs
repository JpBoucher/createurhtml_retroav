﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Youtube_Omnivox
{
    public partial class Form1 : Form
    {
        private string _pathExcel;
        private bool _confirmationExcel = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                _pathExcel = ofd.FileName;
                _confirmationExcel = true;
            }
        }

        private void btnHTML_Click(object sender, EventArgs e)
        {
            OfficeOpenXml.ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            if (_confirmationExcel)
            {
                using (var excelPack = new OfficeOpenXml.ExcelPackage())
                {
                    using (var stream = File.OpenRead(_pathExcel))
                    {
                        excelPack.Load(stream);
                    }

                    var ws = excelPack.Workbook.Worksheets[0];

                    for (int rowNum = 1; rowNum <= ws.Dimension.End.Row; rowNum++)
                    {
                        var wsRow = ws.Cells[rowNum, 1, rowNum, 2];

                        string DA = ws.Cells[rowNum, 1].Text;
                        using (FileStream stream = File.Create(_pathExcel.Substring(0, _pathExcel.LastIndexOf('\\')) +
                                                               "\\" + DA + ".html"))
                        {
                            using (StreamWriter sw = new StreamWriter(stream))
                            {
                                sw.Write("<html><body><a href=\"" + ws.Cells[rowNum, 2].Text +
                                         "\">Correction Tp</a></body></html>");
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Veuillez sélectionner un fichier excel avec le bouton de gauche");
            }
        }

    }
}